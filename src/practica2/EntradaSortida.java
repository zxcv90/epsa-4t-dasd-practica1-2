package practica2;
import java.io.*;
import java.util.*;

public class EntradaSortida{
	
  public static void main(String args[]) throws IOException {
System.out.println("*****************************************************************\n");
System.out.println("*DASD - 2015			Jose Torres Penalva (jotorpe8@epsa.upv.es)*\n");
System.out.println("*****************************************************************\n");
	  
    int j, i, f;
    byte[] buffer= new byte[80];
    String filename, filename2;
    float f1= (float) 3.1416;
    float f2= 0;
    float fpropi1= 0;
    String fpropi2= "";
    
    try {
      // E/S amb InputStream i OutputStream
      System.out.println("-Teclege una cadena: ");
      j= System.in.read(buffer);
      System.out.print("-La cadena: ");
      System.out.write(buffer,0,j);
      System.out.print("\n");
      // Conversi� de la cadena de bytes a cadena de caracters (2 bytes)
      String tira= new String(buffer,0,j);
      System.out.println("-Altra vegada la cadena: " + tira);
      System.out.print("\n");
      // E/S amb BufferedReader y PrintWriter
      // Convenient amb cadenes de caracters (1 caracter = 2 bytes)
      BufferedReader stdIn= new BufferedReader
          (new InputStreamReader(System.in));
      PrintWriter stdOut= new PrintWriter(System.out);
      // E/S amb InputStream i OutputStream
/* COMPLETAR: Llegir un enter per teclat i escriure'l en pantall */
/* 1 */
      System.out.println("-Introdueixca un enter: ");
      i= System.in.read(buffer);
      System.out.print("-L'enter es: ");
      System.out.write(buffer,0,i);
      System.out.print("\n");
/* 1 */
      // E/S amb BufferedReader i PrintWriter
      // Convenient amb cadenes de caracters (1 caracter = 2 bytes)
      System.out.println("-Teclege un nom per a un fitxer: ");
      f= System.in.read(buffer);
      filename= new String(buffer, 0, f);
      System.out.println("-Nom de fitxer: " + filename);
      System.out.print("\n");
      
/* COMPLETAR: Llegir una cadena per al nom del titxer des del teclat
 * emmagamatzemar-la a la variable filename, i mostrar la variable per pantalla
 */
/* 2 */
      String fB;
      Scanner lector= new Scanner(System.in);
      System.out.println("-Introduixca un float al fitxer: ");
      fpropi1= lector.nextFloat();
      System.out.println("-El valor flotant introduit es: ");
      System.out.printf("%.3f", fpropi1);
      System.out.println("/n");
      
      int intBits = Float.floatToIntBits(fpropi1); 
      String binary = Integer.toBinaryString(intBits);
      
      System.out.println("-El valor flotant en format binari es: ");
      System.out.println(binary);
      lector.close();
      
      System.out.println("-Fitxer 1: "+ filename);
      PrintWriter fout= new PrintWriter(new FileOutputStream(filename));
      /*BufferedReader fin= new BufferedReader
              (new InputStreamReader(new FileInputStream(filename)));
          fout.println(new Float(fpropi1).toString()); fout.flush();*/
      BufferedReader fin= new BufferedReader
    		  (new InputStreamReader(new FileInputStream(filename)));
      	fout.println(binary); fout.flush();
          
          fpropi2= fin.readLine();
          System.out.println("-Escrit i llegit el float: " + fpropi2 +
                  " del fitxer 1: " + filename);
/* 2 */
// E/S amb fitxers i floats en format num�ric
/* COMPLETAR: Escriga un float al fitxer filename (en format binari)
 * Llija el float que ha escrit al fitxer filename i mostre'l per pantalla
 * AJUDA: Mire el codi de m�s avall, que �s paregut per� en format de text
 *//* 3 */
/* 3 */
// E/S amb fitxers i floats en format de text
      filename2=filename + ".txt";
      System.out.println("-Fitxer 2: "+ filename2);
      //fout= new DataOutputStream(new FileOutputStream(filename2));
      //fin= new DataInputStream(new FileInputStream(filename2));
      //fout.writeBytes(new Float(f1).toString()+"\n");
      //f2= Float.valueOf(fin.readLine()).floatValue();
      PrintWriter fout2= new PrintWriter(new FileOutputStream(filename2));
      
      BufferedReader fin2= new BufferedReader
          (new InputStreamReader(new FileInputStream(filename2)));
      fout2.println(new Float(f1).toString()); fout2.flush();
      
      //System.out.println(fin2.readLine());
      f2= Float.valueOf(fin2.readLine()).floatValue();
      System.out.println("-Escrit i llegit el float: " +f2+
          " del fitxer 2: " +filename2);
      
      fout.close();
      fin.close();
      fout2.close();
      fin2.close();
    } catch (IOException e) {
      System.out.println("Error en E/S");
      System.exit(1);
    } 
  }
}
