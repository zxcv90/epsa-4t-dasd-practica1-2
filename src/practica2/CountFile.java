package practica2;
import java.io.*;

public class CountFile {
	
  public static void main (String[] args)
    throws java.io.IOException, java.io.FileNotFoundException {
    int count= 0;
    InputStream is= null;
    String filename;
    
    if (args.length >= 1) {
/* COMPLETAR: is= Cree una instància de FileInputStream
 * per a llegir del fitxer passat com argument a args[0]
 */
/* 1 */
      filename= args[0];
      //filename = "/home/zxcv90/robots.txt";
      is= new FileInputStream(filename);
/* 1 */
    } else {
      is= System.in;
      filename= "Input";
    }
/* COMPLETAR: Utilitze amb while (...) count++;
 * un mètode de FileInputSream per a llegir un caracter
 */
/* 2 */
    System.out.println("Tamany total per a lectura: " + is.available());
    while(is.available() != 0){
    	try {
      	  int content= 0;
      	  content= is.read();
      	  System.out.print((char) content);
      	  //System.out.print("\n");
      	  count++;
  	} catch (IOException e) {
  		e.printStackTrace();
  	} finally{
  		try {
  			if(is == null)
  				is.close();
  		} catch (IOException ex) {
  			ex.printStackTrace();}
  			}
    }
/* 2 */
    System.out.print("\n");
    System.out.println(filename + " has " + count + " chars.");
  }
}
