/*
    Example from Sun Java I/O tutorial
    
    Modified to remove deprecated method: readLine(), and,
        provide a cleaner looking display
    Probably a better way to handle the DecimalFormat values;
        I'm creating to many unnecessary references
        but it works for this simple example
 */
package practica2;
import java.io.*;
import java.text.DecimalFormat;     // to format numbers

/* Es tracta d'escriure en un fitxer amb format les línies amb valors passats
 * més avall de preu unitari, unitats i descripció
 * i després cal llegir aquestes línies del fitxer i treure-les per pantalla
 * Nota: Les dades numériques deuen escriure's com "números" i no com cadenes
 * mentre els Strings han d'escriure's com cadenes de bytes i no com cadenes
 * de caracters (1 caracter = 2 bytes)
 */

public class DataIOTest {
  public static void main(String[] args) throws IOException {
	  System.out.println("*****************************************************************");
	  System.out.println("*DASD - 2015		Jose Torres Penalva (jotorpe8@epsa.upv.es)*");
	  System.out.println("*****************************************************************");
 
	DataOutputStream out= new DataOutputStream (new FileOutputStream("invoice.txt"));

    double[] prices= { 19.99, 9.99, 15.99, 3.99, 4.99 };
    int[] units= { 12, 8, 13, 29, 50 };
    String[] descs = { "Java T-shirt",
                       "Java Mug",
                       "Duke Juggling Dolls",
                       "Java Pin",
                       "Java Key Chain" };
    
    for (int i= 0; i < prices.length; i++) {
      out.writeDouble(prices[i]);
      out.writeChar('\t');
      out.writeInt(units[i]);
      out.writeChar('\t');
      out.writeChars(descs[i]);
      out.writeChar('\n');
    }
    out.close();
    
    DataInputStream in= new DataInputStream(new FileInputStream("invoice.txt"));

    int unit;
    double price;
    double total= 0.0;
    char ch;
    String desc= "";
    StringBuffer outputBuffer= new StringBuffer(32);
    DecimalFormat df= new DecimalFormat("###.##");   
    System.out.println("You've ordered:\n");      
    
    char lineSep = System.getProperty("line.separator").charAt(0);
    
    try {
      while (true) {
    	
    	price= in.readDouble();
        in.readChar();       
        unit= in.readInt();
        in.readChar();
        
        StringBuffer descBuffer = new StringBuffer(32);
	    while ((ch = in.readChar()) != '\n')
	    	descBuffer.append(ch);
	    desc= descBuffer.toString();
        
        printInColumn(Integer.toString(unit), 10, 1);
        printInColumn(desc, 25, 0);
        printInColumn(Double.toString(price), 10, 2);
        System.out.println();                          
        total= total + unit * price;
      }
    } catch (EOFException e) {}    
    System.out.println("\nFor a TOTAL of: $" + df.format(total));
    in.close();
  }    
  static void printInColumn(String str, int col, int flag) {
    String s;
    int length;
    DecimalFormat dfRnd= new DecimalFormat("#00.##");    
    DecimalFormat dfInt= new DecimalFormat("00");           
    switch (flag) {
      case 1:         // argument is an int
        s= "\t" + dfInt.format(Integer.parseInt(str));
        length= s.length();       
        break;
      case 2:         // argument is a double
        s= dfRnd.format(Double.parseDouble(str));
        length= s.length();
        break;
      default:
        s= str;
        length= str.length();
    }
    System.out.print(s);    
    for (int p= length; p < col; ++p) {
      System.out.print(" ");
    }
  }
}
